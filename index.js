

function calculateGrade(mark) {
   
   if (mark >= 90) {
        return 'A';
    } else if (mark >= 80) {
        return 'B';
    } else if (mark >= 70) {
        return 'C';
    } else if (mark >= 60) {
        return 'D';
    } else {
        return 'F';
    }
}

const mark = parseInt(prompt("Enter the student's mark: "));
const grade = calculateGrade(mark);
console.log("The grade of the student is: " + grade);